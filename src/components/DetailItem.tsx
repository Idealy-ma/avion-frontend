import { IonItem, IonIcon, IonLabel } from "@ionic/react";
import { informationCircle, paperPlane, rocket } from "ionicons/icons";

interface Liste{
    information: any;
}

const DetailItem: React.FC<Liste> = ({ information })=>{
    return (
        <IonItem>
            <IonLabel>
                    <p><b>Date :</b> {information.date}</p>
                    <p><b>Debut :</b> {information.debut}</p>
                    <p><b>Fin :</b> {information.fin}</p>
            </IonLabel>
        </IonItem>
    );
}

export default DetailItem;